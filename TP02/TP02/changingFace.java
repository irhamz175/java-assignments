package TP02;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color; 
import javafx.stage.Stage; 
import javafx.scene.control.Button;
import static javafx.geometry.Pos.*;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class changingFace extends Application {
	public void start(Stage stage) {
		//button handler
		Button btnSmiling = new Button("Smling");
		Button btnSad = new Button("Sad");
		Button btnPoker = new Button("Poker");
		Button btnSurprised = new Button("Surprised");
		Button btnLaugh = new Button("Laugh");
		HBox btn = new HBox(12,btnSmiling,btnSad,
							btnPoker,btnSurprised,btnLaugh);
		// create and configure the main circle for the face
		Circle face = new Circle(125, 125, 80);
		face.setFill(Color.YELLOW);
		face.setStroke(Color.RED);
		face.setVisible(false);										//false to hide the face
		
		//create both normal eyes
		Circle rightBig = new Circle(86,100,10);					// outer right eye			
		rightBig.setFill(Color.WHITE);
		rightBig.setStroke(Color.BLUE);
		rightBig.setVisible(false);
		Circle rightEye = new Circle(86, 100, 5);					// pupil right eye
		rightEye.setFill(Color.BLACK);
		rightEye.setStroke(Color.BLUE);
		rightEye.setVisible(false);	
		Circle leftBig = new Circle(163,100,10);					// outer left eye
		leftBig.setFill(Color.WHITE);
		leftBig.setStroke(Color.BLUE);
		leftBig.setVisible(false);
		Circle leftEye = new Circle(163,100,5); 					//pupil left eye
		leftEye.setFill(Color.BLACK); 
		leftEye.setStroke(Color.BLUE);
		leftEye.setVisible(false);									
		
		//create the smile
		Arc mouth = new Arc(125, 150, 45, 35, 0, -180);
		mouth.setFill(Color.YELLOW);
		mouth.setStroke(Color.BLACK);
		mouth.setType(ArcType.OPEN);
		mouth.setVisible(false);
		
		//create the laugh
		Arc laugh = new Arc(125, 150, 45, 35, 0, -180);
		laugh.setFill(Color.RED);
		laugh.setStroke(Color.PINK);
		laugh.setType(ArcType.OPEN);
		laugh.setVisible(false);
		
		//create the sad mouth
		Arc sad = new Arc(125,160,40,30,0,180);
		sad.setFill(Color.YELLOW);
		sad.setStroke(Color.BLACK);
		sad.setType(ArcType.OPEN);
		sad.setVisible(false);
		
		//create both poker eyes
		Line pokerRight = new Line(75,110,100,110);
		pokerRight.setFill(Color.YELLOW);
		pokerRight.setVisible(false);
		Line pokerLeft = new Line(153,110,178,110);
		pokerLeft.setFill(Color.YELLOW);
		pokerLeft.setVisible(false);
		
		//create poker mouth
		Line poker = new Line(110,160,140,160);
		poker.setFill(Color.YELLOW);
		poker.setVisible(false);
		
		//create both surprised eyes
		Circle surprisedRight = new Circle(86,100,10);					
		surprisedRight.setFill(Color.BLACK);
		surprisedRight.setStroke(Color.BLUE);
		surprisedRight.setVisible(false);
		Circle surprisedLeft = new Circle(163,100,10);				
		surprisedLeft.setFill(Color.BLACK);
		surprisedLeft.setStroke(Color.BLUE);
		surprisedLeft.setVisible(false);
		
		//create surprised mouth
		Circle mouthSurprised = new Circle(125,160,15);
		mouthSurprised.setFill(Color.RED);
		mouthSurprised.setStroke(Color.BLUE);
		mouthSurprised.setVisible(false);
		Text message = new Text();
		message.setFill(Color.DARKORANGE);
		message.setFont(Font.font ("Muli", 20));
		
		//set button action based on events
		btnSmiling.setOnAction(event ->{
			message.setText("Smiling Face");
			face.setVisible(true);
			rightBig.setVisible(true);
			rightEye.setVisible(true);	
			leftBig.setVisible(true);
			leftEye.setVisible(true);
			mouth.setVisible(true);
			sad.setVisible(false);
			pokerRight.setVisible(false);
			pokerLeft.setVisible(false);
			poker.setVisible(false);
			laugh.setVisible(false);
			surprisedRight.setVisible(false);
			surprisedLeft.setVisible(false);
			mouthSurprised.setVisible(false);
		});
		btnSad.setOnAction(event ->{
			message.setText("Sad Face");
			face.setVisible(true);
			rightBig.setVisible(true);
			rightEye.setVisible(true);	
			leftBig.setVisible(true);
			leftEye.setVisible(true);
			sad.setVisible(true);
			mouth.setVisible(false);
			pokerRight.setVisible(false);
			pokerLeft.setVisible(false);
			poker.setVisible(false);
			laugh.setVisible(false);
			surprisedRight.setVisible(false);
			surprisedLeft.setVisible(false);
			mouthSurprised.setVisible(false);
		});
		btnPoker.setOnAction(event ->{
			message.setText("Poker Face");
			face.setVisible(true);
			pokerRight.setVisible(true);
			pokerLeft.setVisible(true);
			poker.setVisible(true);
			rightBig.setVisible(false);
			rightEye.setVisible(false);	
			leftBig.setVisible(false);
			leftEye.setVisible(false);
			laugh.setVisible(false);
			mouth.setVisible(false);
			sad.setVisible(false);
			surprisedRight.setVisible(false);
			surprisedLeft.setVisible(false);
			mouthSurprised.setVisible(false);
		});
		btnLaugh.setOnAction(event ->{
			message.setText("Laughing Face");
			face.setVisible(true);
			rightBig.setVisible(true);
			rightEye.setVisible(true);	
			leftBig.setVisible(true);
			leftEye.setVisible(true);
			laugh.setVisible(true);
			mouth.setVisible(false);
			sad.setVisible(false);
			pokerRight.setVisible(false);
			pokerLeft.setVisible(false);
			poker.setVisible(false);
			surprisedRight.setVisible(false);
			surprisedLeft.setVisible(false);
			mouthSurprised.setVisible(false);
		});
		btnSurprised.setOnAction(event ->{
			message.setText("Surprised Face");
			face.setVisible(true);
			surprisedRight.setVisible(true);
			surprisedLeft.setVisible(true);
			mouthSurprised.setVisible(true);
			pokerRight.setVisible(false);
			pokerLeft.setVisible(false);
			poker.setVisible(false);
			rightBig.setVisible(false);
			rightEye.setVisible(false);	
			leftBig.setVisible(false);
			leftEye.setVisible(false);
			laugh.setVisible(false);
			mouth.setVisible(false);
			sad.setVisible(false);

		});
		
		Group emotions = new Group(face,surprisedRight,surprisedLeft,mouthSurprised,
									pokerRight,pokerLeft,poker,rightBig,
									rightEye,leftBig,leftEye,mouth,sad,laugh);


		HBox emotionSquare = new HBox(emotions);
		emotionSquare.setAlignment(CENTER);
		VBox root = new VBox(17,btn,emotionSquare,message);
		root.setAlignment(CENTER);
		Scene scene = new Scene(root,300,250);
		stage.setTitle("Changing Face");
		stage.setScene(scene);
		stage.sizeToScene();
		stage.show();
	}
	
	
	
	public static void main(String[] args) {
		launch(args);
	}

}
