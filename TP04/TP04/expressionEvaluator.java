package TP04;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import java.util.Stack; 
import java.util.StringTokenizer;


public class expressionEvaluator extends Application {
	private static Text postfix= new Text("");
	private static Text nilai = new Text("");
	
	public void start(Stage primaryStage) {
		Text infixText, postfixText,  nilaiText; 
		
		TextField textField = new TextField(); 
        infixText = new Text("Ekspresi Infix: ");
        //HBox infixBox = new HBox(infixText,textField);
        
        postfixText = new Text("Ekspresi Postfix: ");

        //HBox postfixBox = new HBox(postfixText, postfix);
        
        nilaiText = new Text("Nilai: ");
        //HBox nilaiBox = new HBox(nilaiText, nilai);
        
        VBox textSide = new VBox(10,infixText, postfixText, nilaiText);
        VBox valueSide = new VBox(5,textField, postfix, nilai);
        HBox hbox = new HBox(8,textSide, valueSide);
        
        FlowPane pane = new FlowPane(hbox);
		pane.setAlignment(Pos.CENTER);
		pane.setStyle("-fx-background-color: salmon");
		Scene scene = new Scene(pane,400,150);
		
		textField.setOnKeyPressed(e->
		 {if (e.getCode().equals( KeyCode.ENTER))
		 try{
		  Converter(textField.getText());
		 }
		 catch(Exception ex) {
		  postfix.setText("Terdapat Kesalahan input");
		  nilai.setText(ex.toString());
		   }
		  });
		primaryStage.setTitle("Infix -> Postfix Evaluator");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	public static int operandPrecedence(String op) { 
		switch (op) { 
		case "+": 
		case "-": return 1; 
		case "*": 
		case "/": return 2;  
		case "$": return 3; 
		} 
		return 0; 
	}
	public static boolean sign(String s) {
		  boolean bool; 
		  switch(s) {
		   case "+":
		   case "-":
		   case "*":
		   case "/":
		   case "$":bool= true; break;
		   default: bool = false; 
		  }
		  return bool;
		 }
	
	public static void Converter(String inflix) {
		String postflix = "";
		StringTokenizer split = new StringTokenizer(inflix);
		Stack<String> stack = new Stack<>();
		
		while (split.hasMoreTokens()) {
			String token= split.nextToken();
			if (sign(token)) {
				while ( !stack.isEmpty() &&
			    operandPrecedence(token)<=operandPrecedence(stack.peek())) {
			     postflix +=stack.pop() + " ";
			    }
			    stack.push(token);    
			   }
			   else if (token =="(") {
			    stack.push(token);
			   }
			   else if (token == ")") {
			    while(!(stack.peek().equals("(")) &&
			      !stack.isEmpty()) {
			     postflix += stack.pop() + " ";
			    }
			    stack.pop();
			   }
			   else if (Integer.valueOf(token) instanceof Integer) {
			    postflix += token + " ";
			   }
			  }
			  while (!stack.isEmpty()) {
			   postflix += stack.pop() + " ";
			  }
			  
			  postfix.setText(postflix);
			  System.out.println(postflix);
			  Long n = finalValue(postflix);
			  nilai.setText(String.valueOf(n));
		}
	public static void operations (Stack<Long> value,String operand) {
		Long oper_1 = value.pop();
		Long oper_2 = value.pop();
		switch(operand) {
		case "+" : value.push(oper_1 + oper_2); break;
		case "-" : value.push(oper_1 - oper_2); break;
		case "*" : value.push(oper_1 * oper_2); break;
		case "/" : value.push(oper_1 / oper_2); break;
		case "$" : value.push((long) Math.pow(oper_1, oper_2)); break;
		default :
		}
	}
	public static long finalValue (String post) {
		Stack<Long> stackPost = new Stack<>();
		StringTokenizer split = new StringTokenizer(post);
		
		while (split.hasMoreTokens()) {
			String token = split.nextToken();
			if (!sign(token)) 
				stackPost.push(Long.valueOf(token));
			else
				operations(stackPost,token);
		}
		return stackPost.pop();
	}
	
	public static void main(String[] args) {
        Application.launch(args);
    }
	
	// Sorry kak blom ada command dan exception,
	// telat banget saya
}

	// oiya blom bisa double digit juga
