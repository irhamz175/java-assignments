package TP03;

  import javafx.application.Application;
  import javafx.scene.Scene;
  import javafx.scene.layout.Pane;
  import javafx.scene.shape.Line;
  import javafx.scene.text.Text;
  import javafx.scene.shape.Polyline;
  import javafx.scene.shape.Circle;
  import javafx.scene.paint.Color;
  import javafx.animation.Interpolator;
  import javafx.animation.PathTransition;
  import javafx.animation.Timeline;
  import javafx.util.Duration;
  import javafx.scene.input.MouseButton;
  import javafx.scene.input.MouseEvent;
  import javafx.stage.Stage;
  
  public class curveAnimation extends Application {
    public void start(Stage primaryStage) {  
      // Create a cosine curve with Polyline
      Polyline curve = new Polyline();
      for (double angle = -630; angle <= 450; angle++) {
        curve.getPoints().addAll(
          angle, Math.cos(Math.toRadians(angle)));
      }
      curve.setTranslateY(150);
      curve.setTranslateX(375);
      curve.setScaleX(0.4);
      curve.setScaleY(70); 
      curve.setStrokeWidth(1.0 / 25);
      curve.setStroke(Color.RED);
      
      // Draw x-axis 
      Line line1 = new Line(20, 150, 540, 150);
      Line line2 = new Line(540, 150, 530, 140);
      Line line3 = new Line(540, 150, 530, 160);
      Text text1 = new Text(530, 130, "X");
  
      // Draw y-axis
      Line line4 = new Line(250, 10, 250, 280);
      Line line5 = new Line(250, 10, 240, 20);
      Line line6 = new Line(250, 10, 260, 20);
      Text text2 = new Text(220, 20, "Y"); 
  
      // text for line number
      Text first = new Text(100,170,"-2π");
      Text second = new Text(170,170,"-π");
      Text third = new Text(253,170,"0");
      Text fourth = new Text(320,170,"π");
      Text fifth = new Text(390,170,"2π");
      Text sixth = new Text(460,170,"3π");
      
      //circle for the animation
      Circle orbitingBall = new Circle(70, 150, 8);
      orbitingBall.setStyle("-fx-stroke: green; -fx-fill: green");			//paint the ball
      
      // Create the path transition
      PathTransition pt = new PathTransition(Duration.millis(4500), curve, orbitingBall);
      pt.setInterpolator(Interpolator.LINEAR);
      pt.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
      pt.setCycleCount(Timeline.INDEFINITE);
      pt.setAutoReverse(true);
   
      // Add nodes to a pane
      Pane pane = new Pane();
      pane.getChildren().addAll(curve, line1, line2, line3, line4,orbitingBall,
        line5, line6, text1, text2,first, second, third , fourth, fifth, sixth);
      
      //action on mouse click
      pane.setOnMouseClicked(e ->{
    	  if(e.getEventType()==MouseEvent.MOUSE_CLICKED){
    	        if(e.getButton()==MouseButton.PRIMARY){
    	        	pt.play();
    	        }else if(e.getButton()==MouseButton.SECONDARY){
    	        	pt.pause();
    	        }
    	  }
      }); 
  
      Scene scene = new Scene(pane, 550, 300);           
      primaryStage.setTitle("TP03: Animasi Bola"); // Set the window title
      primaryStage.setScene(scene); // Place the scene in the window
      primaryStage.show(); // Display the window
    }

    public static void main(String[] args) {
      launch(args);
    }
  }