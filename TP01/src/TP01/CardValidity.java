package TP01;

import javax.swing.JOptionPane;

public class CardValidity {

	public static void main(String[] args) {
		String phrase;
		phrase = JOptionPane.showInputDialog(null,											//show message interaction for inputs
				 "Enter a card number as a long integer,\nType quit to end:",
				 "Validation of Credit/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);

		while (!phrase.equalsIgnoreCase("quit")) {
			long number =Long.parseLong(phrase);											//convert the phrase into a long type
			
			//validate the card digits with isValid method
			if (isValid(number) == true) {													
				JOptionPane.showMessageDialog(null,
						 "The number " + number + " is valid", "Validation of Credit/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);
				
			}else {
				JOptionPane.showMessageDialog(null,
						 "The number " + number + " is invalid", "Validation of Credit/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);
			}
			
			//Get phrase for next time
			 phrase = JOptionPane.showInputDialog(null,
			 "Enter a card number as a long integer,\nType quit to end:",
			 "Validation of Credit/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);
		}

	}
	public static boolean isValid(long number) {
		if (getSize(number) >= 13 && getSize(number)<=16 && prefixMatched(number,5) ||				//conditions for the card pattern
				prefixMatched(number,4) || prefixMatched(number,6) || prefixMatched(number,37)  ) {
			return ((sumOfDoubleEvenPlace(number) + sumOfOddPlace(number))%10 == 0);				//return true if its valid	
		}return false;
	
	}
	public static int getDigit(int number) {
		if (number<9) {	
			return number;													//return the digit itself
		}return (1 + number%10);											//return the sum between each two digits
		
	}
	public static long sumOfDoubleEvenPlace(long number) {
		long sum = 0;														//temporary container
		String second = number +""; 										//convert the number into a string 
		String pin;
		for (int i= getSize(number)-2 ;0 <= i;i = i-2 ) {
			pin = second.charAt(i) + "";									//convert the char into a string
			sum =sum + getDigit(Integer.parseInt(pin)*2);					//sum the double even digit 	
		}return sum;														//return total sum
	
	}
	public static long sumOfOddPlace(long number) {
		int sum = 0;														//temporary container
		String rest = number+"";											//convert the number into a string
		String pin;
		for (int i = getSize(number)-1 ; 0<=i ; i -= 2) {
			pin = rest.charAt(i) +"";										//convert the char into a string
			sum += Integer.parseInt(pin);									//sum the odd digit
		}return sum;														//return total sum
		
	}
	public static int getSize(long number) { 
        String size = number + ""; 											//convert the number into a string
        return size.length(); 												//return the length of the user's input
	}
	public static long getPrefix(long number, int k){						
            return (long) (number/Math.pow(10, (getSize(number) - k)));									//Return the first digit from number.  	
        														//if number of digits in number is less than k, return number.
  
    }
	public static boolean prefixMatched(long number, int d) {
		return getPrefix(number,getSize(d)) == d;									//return true if the digit d is a prefix for number
	}
	
}
